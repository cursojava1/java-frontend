
package py.com.pol.fpuna.university.model;

import java.io.Serializable;

/**
 *
 * @author Gino Junchaya
 */

public class Alumno implements Serializable {
    
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;
    private String nick;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Override
    public String toString() {
        return "Alumno(id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + ")";
    }
    
}
