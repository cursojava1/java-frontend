package py.com.pol.fpuna.university.client;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.pol.fpuna.university.model.Alumno;

/**
 *
 * @author Gino Junchaya
 */

@Named
@ApplicationScoped
public class StudentClient {
    
    public List<Alumno> findAll(){
        Client client = ClientBuilder.newClient(); 
        List<Alumno> students = client.target("http://localhost:8080/cursojava/rest/students")
        .request().get(new GenericType<List<Alumno>>(){});  
        System.out.println("Received the following students information:" + students);  
        return students;
    }
    
    public Boolean save(Alumno alumno){
        Client client = ClientBuilder.newClient();
        Response response = client.target("http://localhost:8080/cursojava/rest/students")
            .request().post(Entity.entity(alumno, MediaType.APPLICATION_JSON_TYPE));
        return response.getStatus() == 201;
    }
    
}
