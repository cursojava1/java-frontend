package py.com.pol.fpuna.university.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.pol.fpuna.university.client.StudentClient;
import py.com.pol.fpuna.university.model.Alumno;

/**
 *
 * @author Gino Junchaya
 */

@Named
@RequestScoped
public class StudentsBean {
    
    @Inject
    private StudentClient studentClient;

    private List<Alumno> students = new ArrayList<>();
    
    private String firstName;
    private String lastName;
    private Integer age;
    private String nick;
    
    @PostConstruct
    public void init(){
        try {
            students = studentClient.findAll();            
        }
        catch(Throwable ex){
            FacesContext.getCurrentInstance()
            .addMessage(
                "errorMessage", 
                new FacesMessage("Error al obtener alumnos: " + ex.getMessage())
            );
            students = new ArrayList<>();
        }
    }
    
    public void save() throws IOException{
        try {
            cleanMessages();
            Alumno alumno = new Alumno();
            alumno.setFirstName(firstName);
            alumno.setLastName(lastName);
            alumno.setAge(age);
            alumno.setNick(nick);
            Boolean saved = studentClient.save(alumno);
            if(!saved){
                FacesContext.getCurrentInstance()
                .addMessage(
                    "errorMessage", 
                    new FacesMessage("Error al registrar el alumno")
                );
            }
            else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("/university-1.0.0/faces/index.xhtml");
            }            
        }
        catch(Throwable ex){
            FacesContext.getCurrentInstance()
            .addMessage(
                "errorMessage", 
                new FacesMessage("Error al registrar alumno: " + ex.getMessage())
            );            
        }
    }
    
    public List<Alumno> getStudents(){
        return this.students;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
    
    private void cleanMessages(){
        Iterator messages = FacesContext.getCurrentInstance().getMessages();
        while(messages.hasNext()){
            messages.next();
            messages.remove();
        }
    }
    
}
